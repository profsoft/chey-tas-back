composer install
bin/console doctrine:database:create
bin/console doctrine:migrations:execute
mkdir -p config/jwt
openssl genrsa -out config/jwt/private.pem -aes256 4096
openssl rsa -pubout -in config/jwt/private.pem -out config/jwt/public.pem
bin/console admin:create:user