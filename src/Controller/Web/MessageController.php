<?php

namespace App\Controller\Web;

use App\Entity\Chat;
use App\Entity\Message;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class MessageController extends AbstractController
{
    /**
     * @param Request $request
     * @param Chat $chat
     *
     * @Route("/message/{id}", name="web_message")
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $chat = $em->getRepository(Chat::class)->find($id);
        $message = new Message();
        $message->setText($request->query->get('text'));
        $message->setChat($chat);
        $message->setUser($this->getUser());
        $message->setCar($chat->getCar());
        $em->persist($message);
        $em->flush();
        return $this->redirectToRoute('web_chat_show', ['id'=>$id]);
    }
}
