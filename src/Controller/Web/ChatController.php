<?php
/**
 * Created by PhpStorm.
 * User: sergey
 * Date: 06/04/2019
 * Time: 19:44
 */

namespace App\Controller\Web;


use App\Entity\Car;
use App\Entity\Chat;
use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Authentication\AuthenticationManagerInterface;
use Symfony\Component\Security\Core\Authentication\Token\PreAuthenticatedToken;
use Symfony\Component\Security\Core\Authentication\Token\RememberMeToken;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Core\Encoder\PasswordEncoderInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoder;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class ChatController extends AbstractController
{
    private $passwordEncoder;
    private $tokenStorage;
    private $authenticationManager;

    public function __construct(
        UserPasswordEncoderInterface $passwordEncoder,
        TokenStorageInterface $tokenStorage,
        AuthenticationManagerInterface $authenticationManager)
    {
        $this->passwordEncoder = $passwordEncoder;
        $this->tokenStorage = $tokenStorage;
        $this->authenticationManager = $authenticationManager;
    }

    /**
     * @param Request $request
     * @param Integer $id
     * @Route("/chat/{id}", name="web_chat")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function createChat(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $car = $em->getRepository(Car::class)->find($id);
        if ($this->getUser() instanceof User) {
            $chats = $em->getRepository(Chat::class)->findBy([
                'user' => $this->getUser(),
                'car' => $car
            ]);
            $chat = end($chats);
        } else {
            $chat = new Chat();
            $chat->setCar($car);
            $user = new User();
            $user->setName('Anon');
            $user->setLogin(md5(microtime()));
            $password = md5(microtime());
            $user->setSalt(md5(time() . $password));
            $user->setPassword($this->passwordEncoder->encodePassword($user, $password));
            $chat->setUser($user);
            $chat->setText($request->getContent());
            $em->persist($chat);
            $em->persist($user);
            $em->flush();
            $unauthenticatedToken = new RememberMeToken(
                $user,
                'main',
                'asd'
            );
            $this->tokenStorage->setToken($unauthenticatedToken);
        }


        return $this->render('web/chat/index.html.twig', ['chat' => $chat,]);
    }

    /**
     * @param Request $request
     * @param Integer $id
     * @Route("/messages/{id}", name="web_chat_show")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public
    function showChat(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $chat = $em->getRepository(Chat::class)->find($id);
        return $this->render('web/chat/show.html.twig', [
            'chat' => $chat,
            'user' => $this->getUser()
        ]);
    }
}
