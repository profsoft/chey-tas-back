<?php
/**
 * Created by PhpStorm.
 * User: sergey
 * Date: 06/04/2019
 * Time: 02:51
 */

namespace App\Helper;


interface AuthorInterface
{
    public function setCreatedAt(int $createdAt);
    public function getCreatedAt(): ?int;
}