<?php

namespace App\Command;

use Doctrine\ORM\EntityManager;
use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;
use Symfony\Component\HttpFoundation\Session\Storage\Handler\PdoSessionHandler;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoder;

class InitProjectCommand extends ContainerAwareCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this->setName('admin:create:user');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $user = new User();

        $helper = $this->getHelper('question');

        $question = new Question(
            'login(admin):',
            'admin'
        );
        $email = $helper->ask($input, $output, $question);
        $user->setLogin($email);

        $question = new Question(
            'password(admin):',
            'admin'
        );
        $password = $helper->ask($input, $output, $question);
        $user->setSalt(md5(time() . $password));
        /** @var UserPasswordEncoder $encoder */
        $encoder = $this->getContainer()->get('security.password_encoder');
        $user->setPassword($encoder->encodePassword($user, $password));

        $this->getManager()->persist($user);

        $this->getManager()->flush();
        $output->writeln('<info>Done</info>');
    }

    /**
     * @return EntityManager
     */
    private function getManager()
    {
        return $this->getContainer()->get('doctrine.orm.entity_manager');
    }
}
